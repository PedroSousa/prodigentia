const routes = [
    {
        path: '/',
        component: () => import('../Pages/Home.vue'),
        name: 'home'
    },
    {
        path: '/people/:id', 
        component: () => import('../Pages/People.vue'),
        name: 'people'
    }
]

export default routes;