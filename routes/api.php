<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PeopleController;
use App\Models\People;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/people', [PeopleController::class, 'index']);
Route::get('/people/{people}', [PeopleController::class, 'view']);
Route::get('/people/{people}/friends', [PeopleController::class, 'friends']);
Route::get('/people/{people}/friends-of-friends', [PeopleController::class, 'friendsOfFriends']);
Route::get('/people/{people}/suggested-friends', [PeopleController::class, 'suggestedFriends']);
Route::get('/people/{people}/recommended-cities', [PeopleController::class, 'recommendedCities']);
