<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    use HasFactory;

    protected $fillable = ['first_name', 'surname', 'age', 'gender'];

    public function directFriends() {
        return $this->hasManyThrough(
            People::class,
            Connection::class,
            'people_id',
            'id',
            'id',
            'connection_id'
        );
    }

    public function friendOfFriends() {
        return $this->whereIn('id', $this->directFriends->pluck('id'))
            ->with('directFriends')
            ->get()
            ->pluck('directFriends')
            ->flatten(1)
            ->where('id', '<>', $this->id)
            ->whereNotIn('id', $this->directFriends->pluck('id'))
            ->unique('id');
    }

    public function sugestedFriends() {
        return $this->whereIn('id', $this->directFriends->pluck('id'))
            ->with('directFriends')
            ->get()
            ->pluck('directFriends')
            ->flatten(1)
            ->groupBy('id')
            ->whereNotIn('id', $this->directFriends->pluck('id'))
            ->reject(function ($value, $key) {
                return count($value) < 2;
            })->flatten(1)
            ->unique('id')
            ->where('id', '<>', $this->id);
    }

    public function cities() {
        return $this->hasMany(City::class);
    }

    public function friendCities() {
        return $this->directFriends()->with('cities');
    }

    public function recommendedCities() {

        $friendCities = [];
        $friends = $this->directFriends()->with('cities')->get();
        foreach($friends as $friend) {
            foreach($friend->cities as $city) {

                if($this->cities->contains('name', $city->name)) {
                    continue;
                }
                
                if(isset($friendCities[$city->name]) && $friendCities[$city->name]['city'] == $city->name) {
                    $friendCities[$city->name] = [
                        'city' => $city->name,
                        'rating' => $city->rating + $friendCities[$city->name]['rating']
                    ];
                } else if(!isset($friendCities[$city->name])) {
                    $friendCities[$city->name] = [
                        'city' => $city->name,
                        'rating' => $city->rating
                    ];
                }
            }
        }
       
        return $friendCities;   
    }
}
