<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\People;

class PeopleController extends Controller
{
    public function home(Request $request) {
        
        if($request->id) {
            People::findOrFail($request->id);
        }
        
        return view('main');
    }

    public function index() {
        return People::all();
    }

    public function view(People $people) {
        return $people;
    }

    public function friends(People $people) {
        return $people->directFriends;
    }

    public function friendsOfFriends(People $people) {
        return $people->friendOfFriends();
    }

    public function suggestedFriends(People $people) {
        return $people->sugestedFriends();
    }

    public function recommendedCities(People $people) {
        return $people->recommendedCities();
    }
}
