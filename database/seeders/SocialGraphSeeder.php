<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use App\Models\People;
use App\Models\Connection;
use App\Models\City;


class SocialGraphSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filePath = storage_path('socialGraph.json');
        $file = file_get_contents($filePath);
        $dataArray = json_decode($file, true);
        
        foreach($dataArray as $data) {
            People::create([
                'first_name' => $data['firstName'],
                'surname' => $data['surname'],
                'age' => Arr::get($data, 'age'),
                'gender' => $data['gender'],
            ]);

            foreach($data['cities'] as $cityName => $cityRating) {
                City::create([
                    'people_id' => $data['id'],
                    'name' => $cityName,
                    'rating' => $cityRating,
                ]);
            }

            foreach($data['connections'] as $connection) {
                Connection::create([
                    'people_id' => $data['id'],
                    'connection_id' => $connection,
                ]);
            }
        }
    }
}
