## Prodigentia - Desafio técnico

How to run


- git clone git@gitlab.com:PedroSousa/prodigentia.git
- cd prodigentia
- cp .env.example to .env
- docker build -t prodigentia .
- docker-compose up 
- visit [SocialGraph](http://127.0.0.1:8000/)


## More

You can also visit [this project online](https://prodigentia.apostacerta.pt).


